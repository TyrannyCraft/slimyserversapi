package com.CreepersHelp.SlimyServers.servers;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public abstract class ServerHandler {

	private final ServerInfo info;
	
	public static enum State {
		STARTED,
		STOPPED,
		INVALID,
		DISABLED;
		
	}
	
	public ServerHandler(ServerInfo sInfo) {
		this(sInfo, false);
	}
	
	public ServerHandler(ServerInfo sInfo, boolean clearLogs) {
		
		if (sInfo == null)
			throw new NullPointerException("Paramater 'sInfo' must not be null!");
		
		this.info = sInfo;
		
		if (sInfo.isNameConflicting())
		
		if (clearLogs)
			clearLogs();
		
		if (sInfo.isAutoStartEnabled())
			start();
	}
	
	/**
	 * Get the server state
	 * @return
	 */
	public abstract State getServerState();
	
	/**
	 * Clear server log files
	 * @param keepLatest True keeps latest log file.
	 */
	public abstract void clearLogs();
	
	/**
	 * Get the instance of this server
	 * @return this instance 
	 */
	public final ServerHandler getInstance() {
		return this;
	}
	
	/**
	 * Get the instance of this server
	 * @return this instance 
	 */
	public ServerInfo getInfo() {
		return this.info;
	}
	
	/**
	 * This starts the server.
	 * @return true if started successfully 
	 */
	public abstract boolean start(final boolean restart);
	
	/**
	 * This starts the server.
	 * @return true if started successfully 
	 */
	public abstract boolean start();
	
	/**
	 * This starts the server.
	 * @return true if started successfully 
	 */
	public abstract boolean restart();
	
	/**
	 * This stops the server.
	 * @return true if stopped.
	 */
	public abstract boolean stop();
	
	/**
	 * This forcefully kills the server.
	 * @return true if stopped. 
	 */
	public abstract boolean forceStop();
	
	/**
	 * This sends a command to the server instance
	 * @param command
	 */
	public abstract void dispatchCommand(String command);
	
	/**
	 * Checks if server is running.
	 * @return
	 */
	public abstract boolean isRunning();
	
	/**
	 * Read from this stream to get this server's console output.
	 * @return this server instance's input stream.
	 */
	public abstract InputStream getInputStream();
	
	/**
	 * Write to this output stream to write commands to server.
	 * @return this server instance's output stream
	 */
	public abstract OutputStream getOutputStream();
	
	/**
	 * Read from this stream to get this server's console output.
	 * @return this server instance's input stream.
	 */
	public abstract InputStream getErrorStream();
	
	/**
	 * This toggles ALL output on/off.
	 */
	public abstract void toggleAllOutput();
	
	/**
	 * This toggles ALL input on/off.
	 */
	public abstract void toggleAllInput();
	
	/**
	 * Toggle input on/off to server for specified InputStream 
	 * @param input
	 */
	public abstract void toggleStream(StreamHandler input);
	
	/**
	 * Adds an input for console input.
	 * @param output
	 */
	public abstract void addStreamHandler(StreamHandler input);
	
	/**
	 * Remove an input stream from this server handler an alternative may be {@link toggleInput(InputStream)}
	 * 
	 * @param output
	 */
	public abstract void removeStreamHandler(StreamHandler input);
	
	/**
	 * Get all InputStreams for this ServerHandler
	 * @return
	 */
	public abstract List<StreamHandler> getAllStreams();
}

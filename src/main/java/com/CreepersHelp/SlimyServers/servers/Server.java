package com.CreepersHelp.SlimyServers.servers;

import java.io.File;

public abstract class Server {

	/**
	 * This is the command to run when stopping the server instance.
	 * @return
	 */
	public abstract String getStopCommand();
	
	/**
	 * This is the number of SECONDS that it waits before killing the instance
	 * @return
	 */
	public int getStopTimeout() {
		return 10;
	}
	
	/**
	 * Get the specified startup command
	 * @return
	 */
	public abstract String getCommand();
	
	/**
	 * Get the name of this server
	 * @return
	 */
	public abstract String getName();
	
	/**
	 * Get the logs directory
	 * @return
	 */
	public abstract File getLogsDir();
	
	/**
	 * Get the version
	 * @return
	 */
	public String getVersion() {return "latest";}

	/**
	 * Check if restarting is enabled for this server type (Default true)
	 * @return
	 */
	public boolean isRestartEnabled() {return true;}
}
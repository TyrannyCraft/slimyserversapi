package com.CreepersHelp.SlimyServers.servers;

import java.io.File;

public class Spigot extends Server {

	@Override
	public String getStopCommand() {
		return "stop";
	}

	@Override
	public String getCommand() {
		return "{java} -jar {workDir}/jars/spigot-" + getVersion() + ".jar";
	}

	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	@Override
	public File getLogsDir() {
		return new File("{serverDir}" + File.separator + "logs");
	}
}

package com.CreepersHelp.SlimyServers.events;

public class Event {
	private final long	time;
	
	/**
	 * Returns invocation time
	 * 
	 * @return Returns timestamp of invocation
	 */
	public final long getTime() {
		return time;
	}
	
	protected Event() {
		this.time = System.currentTimeMillis() / 1000;
	}
	
	/**
	 * Returns event name
	 * 
	 * @return Returns event name
	 */
	public String getEventName() {
		return this.getClass().getSimpleName();
	}
	
}
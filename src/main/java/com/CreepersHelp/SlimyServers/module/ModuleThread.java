package com.CreepersHelp.SlimyServers.module;

import java.lang.Thread.State;
import java.lang.reflect.Method;

import com.CreepersHelp.SlimyServers.SlimyServers;
import com.CreepersHelp.SlimyServers.plugin.Plugin;
import com.CreepersHelp.SlimyServers.plugin.PluginThread;

/**
 * Safe thread. Interrupted when module is unloaded
 * 
 * @author Encorn
 *
 */
public abstract class ModuleThread {
	private pThread				thread;
	
	public ModuleThread(Module Module) {
		this.thread = new pThread(this, Module);
	}
	
	public final void start() {
		thread.start();
	}
	
	@SuppressWarnings("deprecation")
	public final void stop() {
		thread.stop();
	}
	
	public final void interrupt() {
		thread.interrupt();
	}
	
	public final void join() throws InterruptedException {
		thread.join();
	}
	
	public final State getState() {
		return thread.getState();
	}
	
	public final boolean isAlive() {
		return thread.isAlive();
	}
	
	public abstract void run();
	
	private class pThread extends Thread {
		private ModuleThread	main;
		private Module			Module;
		
		private pThread(ModuleThread main, Module pl) {
			this.main = main;
			this.Module = pl;
		}
		
		@Override
		public void run() {
			try {
				Method add = SlimyServers.getInstance().getPluginManager().getClass().getDeclaredMethod("addThread", Plugin.class, PluginThread.class);
				Method remove = SlimyServers.getInstance().getPluginManager().getClass().getDeclaredMethod("removeThread", Plugin.class, PluginThread.class);
				add.setAccessible(true);
				add.invoke(SlimyServers.getInstance().getModuleManager(), Module, main);
				add.setAccessible(false);
				main.run();
				remove.setAccessible(true);
				remove.invoke(SlimyServers.getInstance().getModuleManager(), Module, main);
				remove.setAccessible(false);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}
}

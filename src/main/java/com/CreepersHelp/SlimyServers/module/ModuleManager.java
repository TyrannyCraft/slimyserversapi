package com.CreepersHelp.SlimyServers.module;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.List;

import com.CreepersHelp.SlimyServers.events.Event;

public abstract class ModuleManager {

	protected abstract void addThread(Module Module, ModuleThread Thread);
	
	protected abstract void killThreadsForModule(Module Module);
	
	protected abstract void removeThread(Module Module, ModuleThread Thread);
	
	/**
	 * Returns list of names of all modules
	 * 
	 * @return Returns list of names of all modules
	 */
	public abstract List<String> getAllModuleNames();
	
	/**
	 * @param Module
	 *            The module
	 * @return ModuleInfo for selected module
	 */
	public abstract ModuleInfo getModuleInfo(Module Module);
	
	/**
	 * Finds module based on its file name
	 * 
	 * @param pl
	 * @return module
	 */
	public abstract Module getModuleByFileName(String pl);
	
	/**
	 * Finds module specified by name
	 * 
	 * @param ModuleName
	 * @return Module or null if not found
	 */
	public abstract Module getModuleByName(String ModuleName);
	
	/**
	 * Unload all modules
	 */
	public abstract void unloadAllModules();
	
	/**
	 * Disable spcified Module
	 * @param ModuleInfo
	 */
	public abstract void DisableModule(ModuleInfo ModuleInfo);
	
	/**
	 * Enable a specified Module
	 * @param ModuleInfo
	 */
	public abstract void EnableModule(ModuleInfo ModuleInfo);
	
	/**
	 * Unload specified ModuleInfo instance
	 * @param ModuleInfo
	 */
	public abstract void unloadModule(ModuleInfo ModuleInfo);
	
	/**
	 * Unloads module
	 * 
	 * @param Module
	 *            Module to be unloaded
	 */
	public abstract void unloadModule(Module Module);
	
	/**
	 * Parse all enabled modules
	 * 
	 * @return Array of loaded modules
	 */
	public abstract Collection<ModuleInfo> getModuleInfos();
	
	/**
	 * Loads and enables a Module
	 * @param filename
	 * @return true if successful
	 */
	public abstract boolean loadModule(String filename);
	
	/**
	 * reLoads and enables a Module
	 * @param filename
	 * @return true if successful
	 */
	public abstract boolean reloadModule(String filename);
	
	/**
	 * reLoads and enables a Module
	 * @param module
	 * @return true if successful
	 */
	public abstract boolean reloadModule(Module mod);
	
	/**
	 * reLoads and enables all Modules
	 * @return true if successful
	 */
	public abstract boolean reloadModules();
	
	/**
	 * Load all modules if not already loaded
	 */
	public abstract void loadAllModules();
	
	/**
	 * Check if a module instance exists
	 * @param Module
	 * @return
	 */
	protected abstract boolean moduleExists(Module Module);
	
	/**
	 * Checks to see if module has a method in specified class
	 * @param Module
	 * @param Method
	 * @param Class
	 * @return
	 */
	protected abstract boolean moduleHasMethod(Module Module, Method Method, Class<?> Class);
	
	/**
	 * Checks if Module is loaded and is safe to call the class
	 * @param Module
	 * @param Class
	 * @return
	 */
	protected abstract boolean moduleIsHandled(Module Module, Class<?> Class);
	
	/**
	 * Checks to see if a class is handled and safe to call
	 * @param Class
	 * @return
	 */
	protected abstract boolean ClassIsHandled(Class<?> Class);
	
	/**
	 * Invoked when event is being dispatched to enabled listeners
	 * 
	 * @param e
	 *            Event to be dispatcher
	 * @param list
	 *            List of enabled modules
	 */
	public abstract void invokeEvent(Event e, List<String> list, boolean override);
	
	/**
	 * Invoked when event is being dispatched to enabled listeners
	 * 
	 * @param e
	 *            Event to be dispatcher
	 * @param list
	 *            List of enabled plugins
	 */
	public final void invokeEvent(Event e) {
		invokeEvent(e, null, true);
	}
	
	/**
	 * Called when method is added to dispatcher
	 * 
	 * @param Module
	 * @param Method
	 * @param Class
	 * @param Instance
	 */
	public abstract void addMethod(Module Module, Method Method, Class<?> Class, Object Instance);
}

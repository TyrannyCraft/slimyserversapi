package com.CreepersHelp.SlimyServers.module;

import java.lang.reflect.Method;

import com.CreepersHelp.SlimyServers.EventHandler;
import com.CreepersHelp.SlimyServers.Listener;
import com.CreepersHelp.SlimyServers.SlimyServers;

public abstract class Module {
	
	/**
	 * Called when plugin is loaded (Before {@link #onEnable()})
	 */
	public void onLoad() {}
	
	/**
	 * Called when all plugins are enabled
	 */
	public abstract void onEnable();
	
	/**
	 * Called before plugin is disabled
	 */
	public void onDisable() {}
	
	/**
	 * Called after {@link #onDisable()}
	 */
	public void onUnload() {}
	
	private final boolean isListener(Class<?> Class) {
		return Listener.class.isAssignableFrom(Class);
	}
	
	/**
	 * Register event listener
	 * 
	 * @param Module
	 *            Module object
	 * @param Instance
	 *            Instance of listener object
	 */
	protected final void addEventListener(Module Module, Object Instance) {
		Class<?> cls = Instance.getClass();
		if (isListener(cls)) {
			Method[] methods = cls.getMethods();
			String MasterClasses = "com.CreepersHelp.API.events.";
			for (Method method : methods) {
				if (!method.isAnnotationPresent(EventHandler.class)) {
					continue;
				}
				Class<?>[] types = method.getParameterTypes();
				if (types.length == 1) {
					Class<?> type = types[0];
					try {
						if (type.getName().toLowerCase().startsWith(MasterClasses.toLowerCase())) {
							if (Class.forName(type.getName()) != null) {
								SlimyServers.getInstance().getModuleManager().addMethod(Module, method, type, Instance);
							}
						}
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
					
				}
			}
		}
	}
	
}

package com.CreepersHelp.SlimyServers.events;

import com.CreepersHelp.SlimyServers.servers.ServerHandler;


public class ServerDispatchCommandEvent extends Event implements Cancellable {

	private final boolean	hasparams;
	private final String[]	params;
	private final String	name;
	private boolean cancelled = false;
	private final ServerHandler serverH;
	
	/**
	 * Parses messages and retrieves first token
	 * 
	 * @return Command name
	 */
	public String getCommandName() {
		return name;
	}
	
	/**
	 * Parses messages and retrieves first token in lowercase
	 * 
	 * @return Command name lowercase
	 */
	public String getCommandNameLowerCase() {
		return getCommandName().toLowerCase();
	}
	
	/**
	 * @return Returns True is command comes with parameters, False if otherwise
	 */
	public boolean hasParameters() {
		return hasparams;
	}
	
	/**
	 * @return Returns Array of parameters or empty array if parameters do not
	 *         exist
	 */
	public String[] getParams() {
		return params;
	}
	
	public ServerDispatchCommandEvent(ServerHandler sH, String message) {
		
		if (message == null || message.length() <= 0) {
			this.hasparams = false;
			this.params = null;
			this.name = null;
			this.cancelled = true;
			this.serverH = null;
					
			return;
		}
		
		if (sH == null) {
			this.hasparams = false;
			this.params = null;
			this.name = null;
			this.cancelled = true;
			this.serverH = null;

			throw new NullPointerException("ServerHandler must not be null!");
		}
		
		while (message.contains("\t"))
			message = message.replace("\t", "    ");
		while (message.startsWith(" "))
			message = message.replace(" ", "");
		while (message.contains("  "))
			message = message.replace("  ", " ");
		this.name = message.split(" ")[0];
		this.hasparams = (name.length() < message.length());
		if (hasparams) {
			this.params = message.substring(name.length() + 1).split(" ");
		} else {
			this.params = new String[0];
		}
		
		this.serverH = sH;
	}

	public ServerHandler getServerHandler() {
		return serverH;
	}

	@Override
	public final boolean isCancelled() {
		return cancelled;
	}

	@Override
	public final void setCancelled(boolean cancel) {
		cancelled = cancel;
	}

}

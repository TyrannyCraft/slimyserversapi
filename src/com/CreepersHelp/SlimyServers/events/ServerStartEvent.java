package com.CreepersHelp.SlimyServers.events;

import com.CreepersHelp.SlimyServers.servers.ServerHandler;

public class ServerStartEvent extends Event implements Cancellable {
	
	private boolean cancelled = false;
	private final ServerHandler serverH;
	
	public ServerStartEvent(ServerHandler serverH) {
		if (serverH == null)
			throw new NullPointerException("Server handler argument must not be null!");
		this.serverH = serverH;
	}
	
	public ServerHandler getServerHandler() {
		return serverH;
	}

	@Override
	public final boolean isCancelled() {
		return cancelled;
	}

	@Override
	public final void setCancelled(boolean cancel) {
		cancelled = cancel;
	}

}

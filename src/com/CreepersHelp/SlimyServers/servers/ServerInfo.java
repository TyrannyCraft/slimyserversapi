package com.CreepersHelp.SlimyServers.servers;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ServerInfo {
	
	private final static Map<ServerInfo, String> names = new HashMap<ServerInfo, String>();
	private final String name;
	private final boolean autoStart;
	private boolean allowRestart;
	private final UUID uuid;
	private final int id;
	private final Server sInfo;
	
	public ServerInfo() {
		this(new Spigot());
	}
	
	public ServerInfo(Server sType) {
		this(sType, "Server" + names.size() + 1);
	}
	
	public ServerInfo(Server sType, String name) {
		this(sType, name, true);
	}
	
	public ServerInfo(Server sType, String name, boolean autoStart) {
		this(sType, name, autoStart, true);
	}
	
	public ServerInfo(Server sType, String name, boolean autoStart, boolean restart) {
		this(sType, name, autoStart, names.size() + 1, restart);
	}
	
	private ServerInfo(Server sType, String name, boolean autoStart, int id, boolean restart) {
		this.name = name;
		
		if (isNameConflicting())
			throw new UnsupportedOperationException("Server names must not be the same!");
		
		this.sInfo = sType;
		this.autoStart = autoStart;
		this.uuid = UUID.randomUUID();
		this.id = id;
		this.allowRestart = restart;
		names.put(this, name);
	}
	
	/**
	 * Check if auto starting is enabled
	 * @return
	 */
	public boolean isAutoStartEnabled() {
		return autoStart;
	}
	
	/**
	 * Get the server.
	 * @return
	 */
	public Server getServer() {
		return sInfo;
	}
	
	/**
	 * Allow auto restarting; providing the server type permits it,
	 * @param restart
	 */
	public void setAutoRestart(boolean restart) {
		allowRestart = restart;
	}
	
	/**
	 * Check if auto restarting is enabling
	 * @return
	 */
	public boolean isAutoRestartEnabled() {
		return sInfo.isRestartEnabled() && allowRestart;
	}
	
	/**
	 * Get the name of this server.
	 * @return
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Check if a name is conflicting
	 * @return true if it is.
	 */
	public final boolean isNameConflicting() {
		return names.containsValue(getName());
	}
	
	/**
	 * Check if a name is conflicting
	 * @return true if it is.
	 */
	public final static boolean isNameConflicting(String name) {
		return names.containsValue(name);
	}
	
	/**
	 * Get the UniqueID of the server
	 * @return
	 */
	public UUID getUUID() {
		return uuid;
	}
	
	/**
	 * Get server ID.
	 * @return int ID
	 */
	public int getID() {
		return id;
	}

}

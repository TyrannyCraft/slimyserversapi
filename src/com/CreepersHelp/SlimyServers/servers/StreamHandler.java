package com.CreepersHelp.SlimyServers.servers;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

public abstract class StreamHandler {
	
	public static enum StreamTypes {
		INPUT,
		OUTPUT;
	}
	
	public static enum Streams {
		PRINTSTREAM,
		INPUTSREAM;
	}
	
	private final StreamTypes sType;
	private final PrintStream ps;
	private final InputStream is;
	private boolean enabled;
	private final boolean disable;
	
	public StreamHandler(OutputStream outputStream, StreamTypes sType, boolean disable) {
		this(new PrintStream(outputStream), sType, disable);
	}
	
	public StreamHandler(PrintStream printStream, StreamTypes sType, boolean disable) {
		if (printStream == null)
			throw new NullPointerException("PrintStream must not be null!");
		
		if (sType == null)
			throw new NullPointerException("StreamType must not be null");
		
		this.ps = printStream;
		this.is = null;
		this.sType = sType;
		this.disable = disable;
	}
	
	public StreamHandler(InputStream inputStream, StreamTypes sType, boolean disable) {
		if (inputStream == null)
			throw new NullPointerException("InputStream must not be null!");
		
		if (sType == null)
			throw new NullPointerException("StreamType must not be null");
		
		this.ps = null;
		this.is = inputStream;
		this.sType = sType;
		this.disable = disable;
	}
	
	public boolean isEnabled() {
		return disable || enabled;
	}
	
	public void disable() {
		enabled = disable || false;
	}
	
	public void enable() {
		enabled = disable || true;
	}
	
	public Streams getType() {
		if (ps != null)
			return Streams.PRINTSTREAM;
		
		if (is != null)
			return Streams.INPUTSREAM;
		
		return null;
	}
	
	public boolean isOutput() {
		return sType == StreamTypes.OUTPUT && sType != StreamTypes.INPUT;
	}

	public boolean isInput() {
		return sType == StreamTypes.INPUT && sType != StreamTypes.OUTPUT;
	}
	
	public Object getStream() {
		if (ps != null)
			return ps;
		
		if (is != null)
			return is;
		
		return null;
	}
}

package com.CreepersHelp.SlimyServers.plugin;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.List;

import com.CreepersHelp.SlimyServers.events.Event;

public abstract class PluginManager {

	protected abstract void addThread(Plugin Plugin, PluginThread Thread);
	
	protected abstract void killThreadsForPlugin(Plugin Plugin);
	
	protected abstract void removeThread(Plugin Plugin, PluginThread Thread);
	
	/**
	 * Returns list of names of all plugins
	 * 
	 * @return Returns list of names of all plugins
	 */
	public abstract List<String> getAllPluginNames();
	
	/**
	 * @param Plugin
	 *            The plugin
	 * @return PluginInfo for selected plugin
	 */
	public abstract PluginInfo getPluginInfo(Plugin Plugin);
	
	/**
	 * Finds plugin based on its file name
	 * 
	 * @param pl
	 * @return plugin
	 */
	public abstract Plugin getPluginByFileName(String pl);
	
	/**
	 * Finds plugin specified by name
	 * 
	 * @param PluginName
	 * @return Plugin or null if not found
	 */
	public abstract Plugin getPluginByName(String PluginName);
	
	/**
	 * Unload all plugins
	 */
	public abstract void unloadAllPlugins();
	
	/**
	 * Disable spcified Plugin
	 * @param PluginInfo
	 */
	public abstract void DisablePlugin(PluginInfo PluginInfo);
	
	/**
	 * Enable a specified Plugin
	 * @param PluginInfo
	 */
	public abstract void EnablePlugin(PluginInfo PluginInfo);
	
	/**
	 * Unload specified PluginInfo instance
	 * @param PluginInfo
	 */
	public abstract void unloadPlugin(PluginInfo PluginInfo);
	
	/**
	 * Unloads plugin
	 * 
	 * @param Plugin
	 *            Plugin to be unloaded
	 */
	public abstract void unloadPlugin(Plugin Plugin);
	
	/**
	 * Parse all enabled plugins
	 * 
	 * @return Array of loaded plugins
	 */
	public abstract Collection<PluginInfo> getPluginInfos();
	
	/**
	 * Loads and enables a Plugin
	 * @param filename
	 * @return true if successful
	 */
	public abstract boolean loadPlugin(String filename);
	
	/**
	 * reLoads and enables a Plugin
	 * @param filename
	 * @return true if successful
	 */
	public abstract boolean reloadPlugin(String filename);
	
	/**
	 * reLoads and enables a Plugin
	 * @param plugin
	 * @return true if successful
	 */
	public abstract boolean reloadPlugin(Plugin mod);
	
	/**
	 * reLoads and enables all Plugins
	 * @return true if successful
	 */
	public abstract boolean reloadPlugins();
	
	/**
	 * Load all plugins if not already loaded
	 */
	public abstract void loadAllPlugins();
	
	/**
	 * Check if a plugin instance exists
	 * @param Plugin
	 * @return
	 */
	protected abstract boolean pluginExists(Plugin Plugin);
	
	/**
	 * Checks to see if plugin has a method in specified class
	 * @param Plugin
	 * @param Method
	 * @param Class
	 * @return
	 */
	protected abstract boolean pluginHasMethod(Plugin Plugin, Method Method, Class<?> Class);
	
	/**
	 * Checks if Plugin is loaded and is safe to call the class
	 * @param Plugin
	 * @param Class
	 * @return
	 */
	protected abstract boolean pluginIsHandled(Plugin Plugin, Class<?> Class);
	
	/**
	 * Checks to see if a class is handled and safe to call
	 * @param Class
	 * @return
	 */
	protected abstract boolean ClassIsHandled(Class<?> Class);
	
	/**
	 * Invoked when event is being dispatched to enabled listeners
	 * 
	 * @param e
	 *            Event to be dispatcher
	 * @param list
	 *            List of enabled plugins
	 */
	public abstract void invokeEvent(Event e, List<String> list, boolean override);
	
	/**
	 * Invoked when event is being dispatched to enabled listeners
	 * 
	 * @param e
	 *            Event to be dispatcher
	 * @param list
	 *            List of enabled plugins
	 */
	public final void invokeEvent(Event e) {
		invokeEvent(e, null, true);
	}
	
	/**
	 * Called when method is added to dispatcher
	 * 
	 * @param Plugin
	 * @param Method
	 * @param Class
	 * @param Instance
	 */
	public abstract void addMethod(Plugin Plugin, Method Method, Class<?> Class, Object Instance);
}

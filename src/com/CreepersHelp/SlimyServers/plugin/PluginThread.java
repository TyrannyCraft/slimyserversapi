package com.CreepersHelp.SlimyServers.plugin;

import java.lang.Thread.State;
import java.lang.reflect.Method;

import com.CreepersHelp.SlimyServers.SlimyServers;

/**
 * Safe thread. Interrupted when plugin is unloaded
 * 
 * @author Encorn
 *
 */

public abstract class PluginThread {
	private pThread				thread;
	
	public PluginThread(Plugin Plugin) {
		this.thread = new pThread(this, Plugin);
	}
	
	public final void start() {
		thread.start();
	}
	
	@SuppressWarnings("deprecation")
	public final void stop() {
		thread.stop();
	}
	
	public final void interrupt() {
		thread.interrupt();
	}
	
	public final void join() throws InterruptedException {
		thread.join();
	}
	
	public final State getState() {
		return thread.getState();
	}
	
	public final boolean isAlive() {
		return thread.isAlive();
	}
	
	public abstract void run();
	
	private class pThread extends Thread {
		private PluginThread	main;
		private Plugin			plugin;
		
		private pThread(PluginThread main, Plugin pl) {
			this.main = main;
			this.plugin = pl;
			this.setUncaughtExceptionHandler(new MyThreadGroup());
		}
		
		@Override
		public void run() {
			try {
				Method add = SlimyServers.getInstance().getPluginManager().getClass().getDeclaredMethod("addThread", Plugin.class, PluginThread.class);
				Method remove = SlimyServers.getInstance().getPluginManager().getClass().getDeclaredMethod("removeThread", Plugin.class, PluginThread.class);
				add.setAccessible(true);
				add.invoke(SlimyServers.getInstance().getPluginManager(), plugin, main);
				add.setAccessible(false);
				main.run();
				remove.setAccessible(true);
				remove.invoke(SlimyServers.getInstance().getPluginManager(), plugin, main);
				remove.setAccessible(false);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		/**
		 * A ThreadGroup that overides the uncaughtException method
		 * @author Bharat Sharma
		 *
		 */
		class MyThreadGroup extends ThreadGroup {
		 
		  public MyThreadGroup()
		  {
		    super("Plugin Group");
		  }
		   
		  // put your exception handling code here.
		  @Override
		  public void uncaughtException(Thread thread, Throwable t) {
			  SlimyServers.getInstance().getPluginManager().DisablePlugin(SlimyServers.getInstance().getPluginManager().getPluginInfo(plugin));
			  System.out.println(t.getMessage());
			  System.out.println("Handled by myThreadGroup!");
		  }
		}
	}
}

package com.CreepersHelp.SlimyServers;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import org.apache.log4j.Logger;

import com.CreepersHelp.SlimyServers.events.Event;
import com.CreepersHelp.SlimyServers.module.Module;
import com.CreepersHelp.SlimyServers.module.ModuleManager;
import com.CreepersHelp.SlimyServers.plugin.Plugin;
import com.CreepersHelp.SlimyServers.plugin.PluginManager;
import com.CreepersHelp.SlimyServers.servers.ServerHandler;
import com.CreepersHelp.SlimyServers.servers.ServerInfo;

/**
 * @author Creepers-Help
 */
public abstract class SlimyServers {
	
	private final Logger logger;
	private static SlimyServers instance = null;
	private final String apiVersion = "Unkown-Version";
    public boolean useJline = true;
    public boolean useConsole = true;
	
	public SlimyServers() {
        if (SlimyServers.instance != null && this != SlimyServers.instance)
            throw new UnsupportedOperationException("Cannot redefine singleton Server");

        SlimyServers.instance = this;
        logger = Logger.getLogger(instance.getClass());
        
        SlimyServers.getInstance().getLogger().info("This server is running version: " + getVersion() + " (Implementing API version " + getApiVersion() + ")");
	}

	/**
	 * Get the current wrapper version
	 * @return
	 */
	public abstract String getVersion();

	/**
	 * Get the logger associated with this instance.
	 * @return Logger associated with this instance.
	 */
	public Logger getLogger() {
		return getInstance().logger;
	}
	
	/**
	 * This returns the current instance of the currently running McHub.
	 * @return Current instance of the currently running McHub. 
	 */
	public final static SlimyServers getInstance() {
		return SlimyServers.instance;
	}

	/**
	 * Get the plugin manager
	 * @return
	 */
	public abstract PluginManager getPluginManager();

	/**
	 * Get the module manager
	 * @return
	 */
	public abstract ModuleManager getModuleManager();
	
	/**
	 * This is to create a new Server.
	 * @param serverBot
	 * @return
	 */
	public abstract ServerHandler addServer(final ServerInfo serverBot);
	
	/**
	 * This is to create a new Server and forcefully start it, even if auto startup is disabled.
	 * @param serverBot
	 * @param forceStart
	 * @return
	 */
	public abstract ServerHandler addServer(final ServerInfo serverBot, final boolean forceStart);
	
	/**
	 * Remove a server handler
	 * @param serverBot
	 */
	public abstract void removeServer(ServerInfo serverBot);
	
	/**
	 * Remove a server handler
	 * @param serverHandler
	 */
	public abstract void removeServer(ServerHandler serverBot);

	/**
	 * Get a server instance
	 * @param sInfo
	 * @return
	 */
	public abstract ServerHandler getServer(final ServerInfo sInfo);
	
	/**
	 * Checks if the current instance is premium
	 * @return
	 */
	public abstract boolean isInstancePremium();
	
	/**
	 * Reloads all modules for the current instance
	 */
	public abstract void reloadModules();
	
	/**
	 * Reloads all plugins for the current instance
	 */
	public abstract void reloadPlugins();
	
	/**
	 * Reloads specified module
	 * @param module
	 */
	public abstract void reloadModule(final Module module);
	
	/**
	 * Reloads specified plugin
	 * @param plugin
	 */
	public abstract void reloadPlugin(final Plugin plugin);
	
	
	private String getApiVersion() {
		return apiVersion;
	}

	public final File getWorkingDir() {
		try {
			return new java.io.File( "." ).getCanonicalFile();
		} catch (IOException e) {
			return null;
		}
	}

	public abstract void invokeEvent(Event e, List<String> list, boolean override);
	
	public final void invokeEvent(Event e) {
		invokeEvent(e, null, true);
	}
	
	public final static String getErrorClass(Throwable e) {
		
		String stackTrace = getStackTraceAsString(e);
		int i = 1;
		String firstLine = stackTrace.split("\n")[i].replace("\t", "");
		while (firstLine.startsWith("at java.") || firstLine.startsWith("at javax.") || firstLine.startsWith("at oracle.") || firstLine.startsWith("at jdk.") || firstLine.startsWith("at sun.")) {
			try {
				i++;
				firstLine = stackTrace.split("\n")[i].replace("\t", "");
				if (i > stackTrace.split("\n").length)
					break;
			} catch (Throwable t) {
				firstLine = stackTrace.split("\n")[1].replace("\t", "");
				break;
			}
		}
		String clazz = "null";
		
		if (firstLine.contains("$")) {
			String[] clazzes = firstLine.split("\\$");
			clazz = clazzes[clazzes.length - 1].split("\\(")[0].split("\\.")[0];
		} else 
			clazz = firstLine.split("\\(")[1].split("\\)")[0].split(".java")[0];
		
		return clazz;
	}
	
	public final static String getStackTraceAsString(Throwable throwable) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw, true);
		throwable.printStackTrace(pw);
		
		return sw.getBuffer().toString();
	}
}

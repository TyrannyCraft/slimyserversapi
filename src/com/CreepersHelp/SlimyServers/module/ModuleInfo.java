package com.CreepersHelp.SlimyServers.module;

import java.io.File;
import java.net.URLClassLoader;

@SuppressWarnings("unused")
public abstract class ModuleInfo {
	
	private final String Author;
	private final File moduleFile;
	private final String Version;
	private final Class<?> Class;
	private final String Name;
	private final URLClassLoader loader;
	private final String desc;
	
	public ClassLoader getLoader() {
		return loader;
	}
	
	protected ModuleInfo(URLClassLoader clazzL, String author, File fileEntry, String version, String name, String desc, Class<?> Class) {
		this.Author = author;
		this.moduleFile = fileEntry;
		this.Version = version;
		this.Class = Class;
		this.Name = name;
		this.loader = clazzL;
		this.desc = desc;
	}
	
	public abstract void closeLoader();
	
	public abstract Module getInstance();
	
	protected abstract void unLoad();
	
	protected abstract void Load();
	
	protected abstract boolean isEnabled();
	
	public abstract void setEnabled(boolean enable);
	
	/**
	 * @return the author
	 */
	public String getAuthor() {
		return Author;
	}
	
	/**
	 * @return the file
	 */
	public File getFile() {
		return moduleFile;
	}
	
	/**
	 * @return the version
	 */
	public String getVersion() {
		return Version;
	}
	
	/**
	 * @return the description
	 */
	public String getDescription() {
		return desc;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return desc;
	}
}
